package jefferyemanuel.org.weathermap.frameworks.dagger.components

import dagger.Component
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.ActivityModule
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.AppModule
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.TestNetworkModule
import jefferyemanuel.org.weathermap.data.repositories.WeatherMapSharedPrefRepo
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModule::class, TestNetworkModule::class, ActivityModule::class))
interface TestAppComponent : AppComponent {
    fun httpClient(): okhttp3.OkHttpClient
    fun sharedPrefs(): WeatherMapSharedPrefRepo
}