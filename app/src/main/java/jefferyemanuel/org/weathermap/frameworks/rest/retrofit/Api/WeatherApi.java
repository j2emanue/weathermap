package jefferyemanuel.org.weathermap.frameworks.rest.retrofit.Api;

import android.support.annotation.NonNull;

import io.reactivex.Observable;
import jefferyemanuel.org.weathermap.domain.model.WeatherDataModel;
import jefferyemanuel.org.weathermap.domain.model.WeatherForcastModel;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface WeatherApi {

    @NonNull
    @GET("data/2.5/weather?units=metric")
    @Headers({"Content-Type:application/json"})
    Observable<WeatherDataModel> getWeatherByCoordinates(
            @Query("lat") double lat, @Query("lon") double lon);

    @NonNull
    @GET("data/2.5/forecast?units=metric")
    @Headers({"Content-Type:application/json"})
    Observable<WeatherForcastModel> fetchWeatherForcastByCoordinates(
            @Query("lat") double lat, @Query("lon") double lon);
}