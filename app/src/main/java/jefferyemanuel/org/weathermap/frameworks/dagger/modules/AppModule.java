package jefferyemanuel.org.weathermap.frameworks.dagger.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.firebase.auth.FirebaseAuth;
import com.patloew.rxlocation.RxLocation;

import org.greenrobot.eventbus.EventBus;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jefferyemanuel.org.weathermap.BuildConfig;
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.qualifiers.ApplicationContext;
import jefferyemanuel.org.weathermap.presentation.view.permissions.PermissionsHelper;

@Module
public class AppModule {

    public static final String UNSECURE_PREFS = "unsecuredSharedPrefs";

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    @ApplicationContext
    public Context provideContext() {
        return application;
    }

    @Singleton
    @Provides
    public SharedPreferences provideSharedPreferences(@ApplicationContext Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Singleton
    @Provides
    @Named(UNSECURE_PREFS)
    public SharedPreferences provideUnsecureSharedPreferences(@ApplicationContext Context context) {
        //useful if we want to keep some shared preferences after a logout, for example. we can clear defaults but keep items that are not secure items
        return context.getSharedPreferences("unsecureDefaults", Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public RxLocation provideRxLocation(@ApplicationContext Context context) {
        RxLocation rxLocation = new RxLocation(context);
        rxLocation.setDefaultTimeout(30, TimeUnit.SECONDS);
        return new RxLocation(context);
    }

    @Provides
    @Singleton
    public EventBus provideEventBus() {
        return EventBus.builder().throwSubscriberException(BuildConfig.DEBUG).installDefaultEventBus();
    }

    @Provides
    public PermissionsHelper providePermissionsHelper(){
        return new PermissionsHelper();
    }

    @Provides
    public FirebaseAuth provideFirebaseAuth() {
        return FirebaseAuth.getInstance();
    }
}
