package jefferyemanuel.org.weathermap.frameworks.dagger.components;


import org.jetbrains.annotations.NotNull;

import javax.inject.Singleton;

import dagger.Component;
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.ActivityModule;
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.AppModule;
import jefferyemanuel.org.weathermap.presentation.view.controllers.authentication.fragment.LoginFragment;
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.activity.MainActivity;
import jefferyemanuel.org.weathermap.presentation.view.application.WeatherApplication;
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.fragments.CurrentWeatherFragment;
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.fragments.ForcastWeatherFragment;


@Singleton
@Component(modules = {AppModule.class, jefferyemanuel.org.weathermap.frameworks.dagger.modules.NetworkModule.class, ActivityModule.class})
public interface AppComponent {

    void inject(WeatherApplication target);

    void inject(MainActivity target);

    void inject(@NotNull CurrentWeatherFragment target);

    void inject(@NotNull ForcastWeatherFragment target);

    void inject(@NotNull LoginFragment target);
}