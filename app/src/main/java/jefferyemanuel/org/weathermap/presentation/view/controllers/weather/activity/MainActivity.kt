package jefferyemanuel.org.weathermap.presentation.view.controllers.weather.activity

import android.Manifest
import android.location.Location
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import jefferyemanuel.org.weathermap.R
import jefferyemanuel.org.weathermap.data.repositories.WeatherMapSharedPrefRepo
import jefferyemanuel.org.weathermap.frameworks.bus.events.LoginSuccessEvent
import jefferyemanuel.org.weathermap.presentation.contracts.weather.WeatherView.MainActivityView
import jefferyemanuel.org.weathermap.presentation.presenters.weather.MainActivityPresenter
import jefferyemanuel.org.weathermap.presentation.view.application.WeatherApplication
import jefferyemanuel.org.weathermap.presentation.view.base.BaseMvpActivity
import jefferyemanuel.org.weathermap.presentation.view.permissions.PermissionsHelper
import jefferyemanuel.org.weathermap.presentation.view.utils.ConnectivityUtils
import jefferyemanuel.org.weathermap.presentation.view.controllers.authentication.fragment.LoginFragment
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.fragments.CurrentWeatherFragment
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.fragments.ForcastWeatherFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.include_toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject


class MainActivity : BaseMvpActivity<MainActivityView, MainActivityPresenter>(), MainActivityView, MultiplePermissionsListener {

    @Inject
    lateinit var mPresenter: MainActivityPresenter

    @Inject
    lateinit var mPermissionsHelper: PermissionsHelper

    @Inject
    lateinit var mAuth: FirebaseAuth

    @Inject
    lateinit var sharedPref: WeatherMapSharedPrefRepo

    @Inject
    lateinit var bus: EventBus

    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
       //i dont think showing rationale here is necessary. the UX is clear enough but you can uncomment if necessary
        // showLoading(false)
        //showError(getString(R.string.permission_rationale) + permissions)
    }

    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
        report?.let {
            var granted = report.areAllPermissionsGranted()
            showLoading(false)
            when (granted) {
                true -> mPresenter.doRetrieveLocation()
                else -> {
                    showError(getString(R.string.denied_permission))
                }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        mPresenter.attachView(this)
    }

    override fun onPause() {
        super.onPause()
        if (bus.isRegistered(this))
            bus.unregister(this)
    }

    override fun onResume() {
        super.onResume()
        bus.register(this)
    }

    private var mLocation: Location? = null

    override fun onLocationUpdate(location: Location?) {
        mLocation = location
        location?.let { initViewWithLocation(it) }
    }


    override fun createPresenter(): MainActivityPresenter {
        return mPresenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as WeatherApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setUpUi()
        // Check if user is signed in (non-null) and update UI accordingly.
        presenter.checkUserSignedIn(mAuth.currentUser)
    }

    private fun setUpUi() {
        setSupportActionBar(mToolbar)
        setDrawerEnabled(false)
        navigationView.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener { menuItem ->
            if (!ConnectivityUtils.isConnected(this@MainActivity)) {
                Toast.makeText(this@MainActivity, getString(R.string.check_connection), Toast.LENGTH_LONG).show()
                return@OnNavigationItemSelectedListener false
            }

            //Checking if the item is in checked state or not, if not make it in checked state
            when (menuItem.isChecked) {
                true -> menuItem.isChecked = false
                else -> menuItem.isChecked = true
            }

            drawerLayout.closeDrawers()

            when (menuItem.itemId) {

                R.id.current_weather ->
                    presenter.doShowCurrentWeatherScreen(mLocation)
                else ->
                    presenter.doShowForcastWeatherScreen(mLocation)
            }
            true
        })

        var actionBarDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.openDrawer, R.string.closeDrawer)
        drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()
    }

    private fun setUpNavigationMenuHeader() {
        mAuth.currentUser?.email?.let {
            navigationView.getHeaderView(0).findViewById<TextView>(R.id.tvEmail).text = it
        }
    }

    override fun signOut() {
        setDrawerEnabled(false)
        sharedPref.clearAll()
        mAuth.signOut()
    }

    private fun setDrawerEnabled(enabled: Boolean) {
        when (enabled) {
            true -> drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            false -> drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }
    }

    override fun beginLocationRequests() {

        when (ConnectivityUtils.isConnected(this)) {
            true -> {
               // showLoading(true)
                mPresenter.doCheckPlayServicesAvailable()
                mPresenter.doRequestMultiplePermissions()
                setUpNavigationMenuHeader()
                setDrawerEnabled(true)
            }
            else -> Toast.makeText(this, getString(R.string.check_connection), Toast.LENGTH_LONG).show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.action_sign_out -> {
                presenter.doSignOut()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun requestMultiplePermissions() {
        mPermissionsHelper.requestMultiplePermissions(this, clMain_container, listOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION))
    }

    private fun initViewWithLocation(location: Location) {
        presenter.doShowCurrentWeatherScreen(location)
    }

    override fun showLogin() {
        val b = Bundle()
        supportFragmentManager.beginTransaction().replace(R.id.contentFramelayout, LoginFragment.newInstance(b), LoginFragment::class.java.name).commit()
    }

    override fun showCurrentWeatherScreen(location: Location) {
        val b = Bundle()
        b.putParcelable(COORDINATES_BUNDLE_ID, location)
        supportFragmentManager.beginTransaction().replace(R.id.contentFramelayout, CurrentWeatherFragment.newInstance(b), CurrentWeatherFragment::class.java.name).commit()
    }

    override fun showForcastWeatherScreen(location: Location?) {
        val b = Bundle()
        b.putParcelable(COORDINATES_BUNDLE_ID, location)
        supportFragmentManager.beginTransaction().replace(R.id.contentFramelayout, ForcastWeatherFragment.newInstance(b), ForcastWeatherFragment::class.java.name).commit()
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onLoginEvent(successEvent: LoginSuccessEvent) {
        beginLocationRequests()
    }


    companion object {
        const val COORDINATES_BUNDLE_ID = "coordinates"
    }
}
