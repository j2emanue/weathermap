package jefferyemanuel.org.weathermap.presentation.view.utils

import android.support.v7.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability


object AppUtils {
     fun runGooglePlayServicesAvailablilityCheck(context: AppCompatActivity) {
        val resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context)
        if (resultCode != ConnectionResult.SUCCESS) {
            //downloads google play services if supported
            GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(context)
        }
    }


}