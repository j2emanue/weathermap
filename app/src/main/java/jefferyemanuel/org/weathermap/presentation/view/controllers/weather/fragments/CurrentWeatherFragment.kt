package jefferyemanuel.org.weathermap.presentation.view.controllers.weather.fragments

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import jefferyemanuel.org.weathermap.BuildConfig
import jefferyemanuel.org.weathermap.R
import jefferyemanuel.org.weathermap.domain.model.WeatherDataModel
import jefferyemanuel.org.weathermap.presentation.view.application.WeatherApplication
import jefferyemanuel.org.weathermap.presentation.view.base.BaseMvpFragment
import jefferyemanuel.org.weathermap.presentation.contracts.weather.WeatherView.CurrentWeatherView
import jefferyemanuel.org.weathermap.presentation.presenters.weather.CurrentWeatherPresenter
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.activity.MainActivity
import kotlinx.android.synthetic.main.current_weather_fragment.*
import java.util.*
import javax.inject.Inject


class CurrentWeatherFragment : BaseMvpFragment<CurrentWeatherView, CurrentWeatherPresenter>(), CurrentWeatherView {

    var mLocation: Location? = null

    @Inject
    lateinit var mPresenter: CurrentWeatherPresenter

    override fun showWeatherUpdate(model: WeatherDataModel?) {
        initView(model)
    }

    private fun initView(model: WeatherDataModel?) {
        val icon = model?.weather?.get(0)?.icon
        val iconUrl = "${BuildConfig.BASE_ENDPOINT}/img/w/$icon.png"

        imageView.setImageURI(iconUrl)

        textView2.text = model?.name
        textView3.text = model?.main?.temp.toString() + "\u00B0"
        textView.text = String.format(Locale.US, getString(R.string.current_weather_formatter), model?.weather?.get(0)?.description)
    }

    override fun createPresenter(): CurrentWeatherPresenter {
        return mPresenter
    }


    companion object {
        fun newInstance(b: Bundle?): CurrentWeatherFragment {
            val frag = CurrentWeatherFragment()
            frag.arguments = b ?: Bundle()
            return frag
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.current_weather_fragment, container, false)
        (activity!!.applicationContext as WeatherApplication).appComponent.inject(this)
        arguments?.let { mLocation = it.getParcelable(MainActivity.COORDINATES_BUNDLE_ID) }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.attachView(this)
        mPresenter.fetchWeatherUpdate(mLocation)

    }
}