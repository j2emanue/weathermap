package jefferyemanuel.org.weathermap.presentation.view.controllers.authentication.fragment

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import com.github.ajalt.timberkt.Timber
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import jefferyemanuel.org.weathermap.R
import jefferyemanuel.org.weathermap.frameworks.bus.events.LoginSuccessEvent
import jefferyemanuel.org.weathermap.presentation.view.application.WeatherApplication
import jefferyemanuel.org.weathermap.presentation.view.base.BaseMvpFragment
import jefferyemanuel.org.weathermap.presentation.contracts.authentication.AuthenticationView.LoginView
import jefferyemanuel.org.weathermap.presentation.presenters.weather.LoginPresenter
import kotlinx.android.synthetic.main.login_fragment.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject


class LoginFragment : BaseMvpFragment<LoginView, LoginPresenter>(), LoginView {

    @Inject
    lateinit var mPresenter: LoginPresenter

    @Inject
    lateinit var mAuth: FirebaseAuth

    private var mGoogleApiClient: GoogleApiClient? = null

    @Inject
    lateinit var bus: EventBus

    override fun createPresenter(): LoginPresenter {
        return mPresenter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mGoogleApiClient?.let { client ->
            when (client.isConnected) {
                true -> activity?.let {
                    client.stopAutoManage(it)
                    client.disconnect()
                }
                false -> {
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.login_fragment, container, false)
        (activity!!.applicationContext as WeatherApplication).appComponent.inject(this)
        setHasOptionsMenu(true)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()

    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.clear()
    }

    private fun initView() {
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        activity?.let {
            mGoogleApiClient = GoogleApiClient.Builder(it).enableAutoManage(it, { Toast.makeText(activity, "login error", Toast.LENGTH_LONG).show() })
                    ?.addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    ?.build()
        }
        googleBtn.setOnClickListener { presenter.doSignIn() }
    }

    override fun signIn() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    companion object {
        const val RC_SIGN_IN = 1
        fun newInstance(b: Bundle?): LoginFragment {
            val frag = LoginFragment()
            frag.arguments = b ?: Bundle()
            return frag
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                presenter.doFirebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Timber.e { "Google sign in failed" }
                Toast.makeText(activity, R.string.login_failed, Toast.LENGTH_LONG).show()
            }

        }
    }

    override fun firebaseAuthWithGoogle(acct: GoogleSignInAccount?) {
        showLoading(true)
        val credential = GoogleAuthProvider.getCredential(acct?.idToken, null)
        activity?.let {
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(it, {
                        showLoading(false)
                        when (it.isSuccessful) {
                            true -> {
                                bus.post(LoginSuccessEvent())
                            }
                            else -> {
                                Toast.makeText(activity, getString(R.string.login_failed), Toast.LENGTH_LONG).show()
                            }
                        }
                    })
        }
    }
}