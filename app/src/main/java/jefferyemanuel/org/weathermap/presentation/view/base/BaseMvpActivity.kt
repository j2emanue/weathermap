package jefferyemanuel.org.weathermap.presentation.view.base

import android.os.Bundle
import android.view.Menu
import com.hannesdorfmann.mosby.mvp.MvpActivity
import com.hannesdorfmann.mosby.mvp.MvpPresenter
import com.yarolegovich.lovelydialog.LovelyInfoDialog
import jefferyemanuel.org.weathermap.R
import jefferyemanuel.org.weathermap.presentation.view.customviews.dialogs.ProgressDialogFragment
import jefferyemanuel.org.weathermap.presentation.base.BaseMvpView
import jefferyemanuel.org.weathermap.presentation.view.utils.AppUtils


abstract class BaseMvpActivity<V: BaseMvpView, P: MvpPresenter<V>> : MvpActivity<V, P>(){


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

     fun showError(res: Int) {
         showError(getString(res))
    }

     fun showError(msg: String?) {
         showLoading(false)
         LovelyInfoDialog(this)
                 .setTopColorRes(R.color.pink)
                 .setIcon(R.mipmap.ic_launcher)
                 .setTitle(R.string.error_title)
                 .setMessage(msg)
                 .show()
    }

     fun showLoading(show: Boolean) {
         val fm  = supportFragmentManager
         var loadingView  =  fm.findFragmentByTag(ProgressDialogFragment::class.java.name)
        loadingView?.let{
             fm.beginTransaction().remove(loadingView).commitAllowingStateLoss()
         }
         if (show) {
            var loadingView2 = ProgressDialogFragment.newInstance(true)
              loadingView2.show(fm, ProgressDialogFragment::class.java.name)
         }
    }

     fun checkPlayServicesIsAvailable() {
        AppUtils.runGooglePlayServicesAvailablilityCheck(this)
    }


     fun proceedToActivity(aClass: Class<*>?, b: Bundle?, clearTask: Boolean, finish: Boolean) {
    }

     fun exit() {
    }


}