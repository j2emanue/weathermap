package jefferyemanuel.org.weathermap.presentation.view.controllers.weather.fragments.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import jefferyemanuel.org.weathermap.BuildConfig
import jefferyemanuel.org.weathermap.R
import jefferyemanuel.org.weathermap.domain.model.Item7
import kotlinx.android.synthetic.main.weather_forcast_item_row.view.*


class ForeCastWeatherRecyclerAdapter(private var dataSource: List<Item7?>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return WeatherHolder(LayoutInflater.from(parent?.context).inflate(R.layout.weather_forcast_item_row, parent, false))
    }

    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as? WeatherHolder)?.bind(dataSource[position])
    }

    class WeatherHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: Item7?) = data?.let {
            with(itemView) {
                tvStatus.text = data.weather?.get(0)?.main
                tvday.text = data.day_formatted
                tvDate.text = data.dt_txt
                data.weather?.get(0)?.icon.let {
                    val iconUrl = "${BuildConfig.BASE_ENDPOINT}/img/w/$it.png"
                    imageView.setImageURI(iconUrl)
                }
            }
        }
    }
}
