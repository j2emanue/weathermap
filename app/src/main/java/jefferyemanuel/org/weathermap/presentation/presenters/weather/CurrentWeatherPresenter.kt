package jefferyemanuel.org.weathermap.presentation.presenters.weather

import android.location.Location
import io.reactivex.disposables.Disposable
import jefferyemanuel.org.weathermap.domain.model.WeatherDataModel
import jefferyemanuel.org.weathermap.presentation.base.BaseMvpPresenter
import jefferyemanuel.org.weathermap.presentation.contracts.weather.WeatherView.CurrentWeatherView
import jefferyemanuel.org.weathermap.domain.usecases.GetWeatherUpdateUsecase
import jefferyemanuel.org.weathermap.domain.usecases.base.DefaultSubscriber
import javax.inject.Inject

class CurrentWeatherPresenter @Inject constructor(private var getWeatherUpdateUsecase: GetWeatherUpdateUsecase) : BaseMvpPresenter<CurrentWeatherView>() {
    fun fetchWeatherUpdate(location: Location?) {
        location?.let {
            with(getWeatherUpdateUsecase) {
                mLocation = location
                execute(object : DefaultSubscriber<WeatherDataModel>() {
                    override fun onSubscribe(d: Disposable) {
                        showLoading(true)
                    }

                    override fun onNext(t: WeatherDataModel) {
                        super.onNext(t)
                        view?.showWeatherUpdate(t)
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        showError()
                    }

                    override fun onComplete() {
                        super.onComplete()
                        showLoading(false)
                    }
                })
            }
        }

    }

}

