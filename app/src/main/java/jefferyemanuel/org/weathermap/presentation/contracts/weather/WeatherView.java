package jefferyemanuel.org.weathermap.presentation.contracts.weather;


import android.location.Location;

import jefferyemanuel.org.weathermap.domain.model.WeatherDataModel;
import jefferyemanuel.org.weathermap.domain.model.WeatherForcastModel;
import jefferyemanuel.org.weathermap.presentation.base.BaseMvpView;


public interface WeatherView {

    interface MainActivityView extends BaseMvpView {

        void requestMultiplePermissions();

        void checkPlayServicesIsAvailable();

        void onLocationUpdate(Location location);

        void showCurrentWeatherScreen(Location location);

        void showForcastWeatherScreen(Location location);

        void beginLocationRequests();

        void showLogin();

        void signOut();
    }

    interface CurrentWeatherView extends BaseMvpView {
        void showWeatherUpdate(WeatherDataModel model);

    }

    interface ForcastWeatherView extends BaseMvpView {
        void showWeatherForcast(WeatherForcastModel model);
    }
}
