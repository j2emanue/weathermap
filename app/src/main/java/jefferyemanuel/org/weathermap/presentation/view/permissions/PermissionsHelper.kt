package jefferyemanuel.org.weathermap.presentation.view.permissions

import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener
import jefferyemanuel.org.weathermap.R


class PermissionsHelper{

    //dont think i need a weak ref here. //todo take a look later
   // private var weakActivityRef : WeakReference<AppCompatActivity>? = null

    fun requestMultiplePermissions(activity: AppCompatActivity, snackbarAnchor: ViewGroup, permissions:List<String>){

        check(activity is MultiplePermissionsListener)
        val snackbarMultiplePermissionsListener = SnackbarOnAnyDeniedMultiplePermissionsListener.Builder
                .with(snackbarAnchor,R.string.you_denied_critical_permissions)
                .withDuration(15000)
                .withOpenSettingsButton(activity.getString(R.string.settings))
                .withCallback(object : Snackbar.Callback() {
                    override fun onShown(snackbar: Snackbar?) {}
                    override fun onDismissed(snackbar: Snackbar?, event: Int) {}
                })
                .build()

         var compositePermissionsListener =  CompositeMultiplePermissionsListener(snackbarMultiplePermissionsListener, activity as MultiplePermissionsListener)

        Dexter.withActivity(activity)
                .withPermissions(permissions)
                .withListener(compositePermissionsListener)
                .onSameThread()
                .check()
    }


}