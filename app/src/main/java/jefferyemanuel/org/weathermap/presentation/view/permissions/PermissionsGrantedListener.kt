package jefferyemanuel.org.weathermap.presentation.view.permissions


//can also use the command pattern but for simple app, this will be ok
interface PermissionsGrantedListener{
   fun onPermissionsGranted(granted:Boolean)

}