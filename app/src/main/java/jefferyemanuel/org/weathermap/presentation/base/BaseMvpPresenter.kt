package jefferyemanuel.org.weathermap.presentation.base

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import jefferyemanuel.org.weathermap.R


public abstract class BaseMvpPresenter<V : BaseMvpView> : MvpBasePresenter<V>() {

    fun showError() {
        view?.showLoading(false)
        view?.showError(R.string.ERROR)
    }

    fun showError(msg: String) {
        view?.showLoading(false)
        view?.showError(msg)
    }

    fun showLoading(show: Boolean) {
        view?.showLoading(show)
    }
}