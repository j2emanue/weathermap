package jefferyemanuel.org.weathermap.presentation.view.application

import jefferyemanuel.org.weathermap.frameworks.dagger.components.DaggerTestAppComponent
import jefferyemanuel.org.weathermap.frameworks.dagger.components.TestAppComponent
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.ActivityModule
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.TestAppModule
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.TestNetworkModule

/**
 * Created by j2emanue on 5/20/17.
 */

class WeatherTestApplication : WeatherApplication() {


    override val appComponent: TestAppComponent by lazy {
        DaggerTestAppComponent
                .builder()
                .appModule(TestAppModule(this))
                .activityModule(ActivityModule())
                .testNetworkModule(TestNetworkModule())
                .build()
    }
}
