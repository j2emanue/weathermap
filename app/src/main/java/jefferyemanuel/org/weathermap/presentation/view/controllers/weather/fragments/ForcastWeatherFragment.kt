package jefferyemanuel.org.weathermap.presentation.view.controllers.weather.fragments

import android.location.Location
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import jefferyemanuel.org.weathermap.R
import jefferyemanuel.org.weathermap.domain.model.WeatherForcastModel
import jefferyemanuel.org.weathermap.presentation.view.application.WeatherApplication
import jefferyemanuel.org.weathermap.presentation.view.base.BaseMvpFragment
import jefferyemanuel.org.weathermap.presentation.contracts.weather.WeatherView
import jefferyemanuel.org.weathermap.presentation.presenters.weather.ForcastWeatherPresenter
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.activity.MainActivity
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.fragments.adapter.ForeCastWeatherRecyclerAdapter
import kotlinx.android.synthetic.main.forcast_weather_fragment.*
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject


class ForcastWeatherFragment : BaseMvpFragment<WeatherView.ForcastWeatherView, ForcastWeatherPresenter>(), WeatherView.ForcastWeatherView {


    @Inject
    lateinit var mPresenter: ForcastWeatherPresenter

    var adapter: ForeCastWeatherRecyclerAdapter? = null
    var mLocation: Location? = null

    override fun createPresenter(): ForcastWeatherPresenter {
        return mPresenter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.forcast_weather_fragment, container, false)
        (activity!!.applicationContext as WeatherApplication).appComponent.inject(this)
        arguments?.let { mLocation = it.getParcelable(MainActivity.COORDINATES_BUNDLE_ID) }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPresenter.attachView(this)
        mPresenter.fetchForcastedWeatherUpdate(mLocation)
    }

    override fun showWeatherForcast(model: WeatherForcastModel?) {
        initView(model)
    }

    private fun initView(model: WeatherForcastModel?) {
       //i prefer single responsibility //todo move toolbar init into  its own method
        mToolbar.title = getString(R.string.forecasted_weather)
        (activity as? AppCompatActivity)?.setSupportActionBar(mToolbar)

        model?.list?.let {

            val dividerItemDecoration = DividerItemDecoration(recyclerView.context,
                    LinearLayoutManager.VERTICAL)
            recyclerView.addItemDecoration(dividerItemDecoration)
            adapter = ForeCastWeatherRecyclerAdapter(it)
            recyclerView.adapter = adapter
        }
    }

    companion object {

        fun newInstance(b: Bundle?): ForcastWeatherFragment {
            val frag = ForcastWeatherFragment()
            frag.arguments = b ?: Bundle()
            return frag
        }
    }
}