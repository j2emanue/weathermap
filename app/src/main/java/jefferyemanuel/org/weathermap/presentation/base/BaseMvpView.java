package jefferyemanuel.org.weathermap.presentation.base;


import android.support.annotation.StringRes;

import com.hannesdorfmann.mosby.mvp.MvpView;




public interface BaseMvpView extends MvpView {

    void showError(@StringRes int e);

    void showError(String msg);

    void showLoading(boolean show);
}
