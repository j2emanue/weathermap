package jefferyemanuel.org.weathermap.presentation.presenters.weather

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import jefferyemanuel.org.weathermap.presentation.base.BaseMvpPresenter
import jefferyemanuel.org.weathermap.presentation.contracts.authentication.AuthenticationView.LoginView
import javax.inject.Inject


class LoginPresenter @Inject constructor() : BaseMvpPresenter<LoginView>() {
    fun doSignIn() {
        view?.signIn()
    }

    fun doFirebaseAuthWithGoogle(account: GoogleSignInAccount?) {
        view?.firebaseAuthWithGoogle(account)
    }

}