package jefferyemanuel.org.weathermap.presentation.presenters.weather


import android.location.Location
import com.google.android.gms.location.LocationRequest
import com.google.firebase.auth.FirebaseUser
import io.reactivex.disposables.Disposable
import jefferyemanuel.org.weathermap.presentation.base.BaseMvpPresenter
import jefferyemanuel.org.weathermap.presentation.contracts.weather.WeatherView
import jefferyemanuel.org.weathermap.domain.usecases.GetLocationUseCase
import jefferyemanuel.org.weathermap.domain.usecases.base.DefaultSubscriber
import javax.inject.Inject


class MainActivityPresenter @Inject constructor(private var getLocationUsecase: GetLocationUseCase) : BaseMvpPresenter<WeatherView.MainActivityView>() {
    private val locationRequest: LocationRequest by lazy {
        LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000)
    }

    fun doCheckPlayServicesAvailable() {
        view?.checkPlayServicesIsAvailable()
    }

    fun doRetrieveLocation() {
        getLocationUsecase
                .setLocationRequest(locationRequest)
                .execute(object : DefaultSubscriber<Location>() {
                    override fun onSubscribe(d: Disposable) {
                        super.onSubscribe(d)
                        showLoading(true)
                    }

                    private var mLocation: Location? = null

                    override fun onNext(location: Location) {
                        super.onNext(location)
                        mLocation = location
                    }

                    override fun onError(e: Throwable) {
                        super.onError(e)
                        showError(e.message.toString())
                    }

                    override fun onComplete() {
                        super.onComplete()
                        showLoading(false)
                        view?.onLocationUpdate(mLocation)
                    }
                })
    }

    fun doRequestMultiplePermissions() {
        view?.requestMultiplePermissions()
    }

    fun doShowCurrentWeatherScreen(location: Location?) {
        view?.showCurrentWeatherScreen(location)
    }

    fun doShowForcastWeatherScreen(location: Location?) {
        view?.showForcastWeatherScreen(location)
    }

    fun checkUserSignedIn(currentUser: FirebaseUser?) {
        when (currentUser) {
            null -> {
                view?.showLogin()
            }
            else -> view?.beginLocationRequests()
        }
    }

    fun doSignOut() {
        view?.signOut()
        view?.showLogin()
    }
}
