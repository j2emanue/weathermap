package jefferyemanuel.org.weathermap.presentation.presenters.weather

import android.location.Location
import io.reactivex.disposables.Disposable
import jefferyemanuel.org.weathermap.domain.model.WeatherForcastModel
import jefferyemanuel.org.weathermap.presentation.base.BaseMvpPresenter
import jefferyemanuel.org.weathermap.presentation.contracts.weather.WeatherView.ForcastWeatherView
import jefferyemanuel.org.weathermap.domain.usecases.GetForcastedWeatherUsecase
import jefferyemanuel.org.weathermap.domain.usecases.base.DefaultSubscriber
import javax.inject.Inject


class ForcastWeatherPresenter @Inject constructor(private var getForcastedWeatherUsecase: GetForcastedWeatherUsecase) : BaseMvpPresenter<ForcastWeatherView>() {

    fun fetchForcastedWeatherUpdate(location: Location?) {

        with(getForcastedWeatherUsecase) {
            this.location = location
            execute(object : DefaultSubscriber<WeatherForcastModel>() {
                override fun onSubscribe(d: Disposable) {
                    super.onSubscribe(d)
                    showLoading(true)
                }

                override fun onNext(t: WeatherForcastModel) {
                    super.onNext(t)
                    view?.showWeatherForcast(t)
                }

                override fun onError(e: Throwable) {
                    super.onError(e)
                    showError()
                }

                override fun onComplete() {
                    super.onComplete()
                    showLoading(false)
                }
            })
        }

    }

}