package jefferyemanuel.org.weathermap.presentation.view.base

import android.os.Bundle
import com.hannesdorfmann.mosby.mvp.MvpFragment
import com.hannesdorfmann.mosby.mvp.MvpPresenter
import com.yarolegovich.lovelydialog.LovelyInfoDialog
import jefferyemanuel.org.weathermap.R
import jefferyemanuel.org.weathermap.presentation.view.customviews.dialogs.ProgressDialogFragment
import jefferyemanuel.org.weathermap.presentation.base.BaseMvpView


abstract class BaseMvpFragment<V: BaseMvpView, P: MvpPresenter<V>> : MvpFragment<V, P>() {

    fun showError(res: Int) {
        showError(getString(res))
    }

    fun showError(msg: String?) {
        showLoading(false)
        LovelyInfoDialog(activity)
                .setTopColorRes(R.color.pink)
                .setIcon(R.mipmap.ic_launcher)
                .setTitle(R.string.error_title)
                .setMessage(msg)
                .show()
    }

    fun showLoading(show: Boolean) {
        val fm  = activity?.supportFragmentManager
        var loadingView  =  fm?.findFragmentByTag(ProgressDialogFragment::class.java.name)
        loadingView?.let{
            fm?.beginTransaction()?.remove(loadingView)?.commitAllowingStateLoss()
        }
        if (show) {
            var loadingView2 = ProgressDialogFragment.newInstance(true)
            fm?.let {loadingView2.show(fm, ProgressDialogFragment::class.java.name)}
        }

    }

    fun showLoading(tag: String?, show: Boolean) {
    }

    fun showLoading(b: Boolean, cancelable: Boolean) {
    }

    fun proceedToActivity(aClass: Class<*>?, b: Bundle?, clearTask: Boolean, finish: Boolean) {
    }

    fun exit() {
    }
}