package jefferyemanuel.org.weathermap.presentation.contracts.authentication;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import jefferyemanuel.org.weathermap.presentation.base.BaseMvpView;


public interface AuthenticationView {
    interface LoginView extends BaseMvpView {
        void signIn();
        void firebaseAuthWithGoogle(GoogleSignInAccount account);
    }
}
