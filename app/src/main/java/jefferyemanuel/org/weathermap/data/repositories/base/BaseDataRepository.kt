package jefferyemanuel.org.weathermap.data.repositories.base


/**
 * Interface defining methods for
 * how the data layer can pass data to and from the Domain layer.
 */


interface BaseDataRepository {
    //IN HERE WE CAN DEFINE STRATEGIES FOR RETRIEVING DATA, local, network, or in memory
}