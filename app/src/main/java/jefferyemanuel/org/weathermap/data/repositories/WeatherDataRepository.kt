package jefferyemanuel.org.weathermap.data.repositories

import android.location.Location
import io.reactivex.Observable
import jefferyemanuel.org.weathermap.data.repositories.base.BaseDataRepository
import jefferyemanuel.org.weathermap.domain.model.WeatherDataModel
import jefferyemanuel.org.weathermap.domain.model.WeatherForcastModel
import jefferyemanuel.org.weathermap.frameworks.rest.retrofit.Api.WeatherApi
import retrofit2.Retrofit
import javax.inject.Inject


class WeatherDataRepository @Inject
constructor(private val mRetrofit: Retrofit): BaseDataRepository {

    fun fetchWeatherDataForLocation(mLocation: Location?): Observable<WeatherDataModel>? {
        val service = mRetrofit.create(WeatherApi::class.java)
        return mLocation?.let { service.getWeatherByCoordinates(it.latitude, it.longitude) }

    }

    fun fetchWeatherForcastData(mLocation: Location?): Observable<WeatherForcastModel>? {
        val service = mRetrofit.create(WeatherApi::class.java)
        return mLocation?.let {
            service
                    .fetchWeatherForcastByCoordinates(it.latitude, it.longitude)
                    //lets insert a day field in the payload for better presentation to user
                    .map({
                        it.list?.forEach { item ->
                            item?.dt?.toLong()?.let { it1 ->
                                val date = java.text.SimpleDateFormat("EEE").format(java.util.Date(it1 * 1000))
                                item.day_formatted = date
                            }
                        }
                        it
                    })

        }


    }
}