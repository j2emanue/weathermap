package jefferyemanuel.org.weathermap.data.repositories;

import android.content.SharedPreferences;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import static jefferyemanuel.org.weathermap.frameworks.dagger.modules.AppModule.UNSECURE_PREFS;

//this will always be a local repository, its just here for convenience
public class WeatherMapSharedPrefRepo {


    private final String DEFAULT = "";
    private final String KEY_EMAIL = "email";

    SharedPreferences sp;

    @Named(UNSECURE_PREFS)
    SharedPreferences unsecure_sp;

    @Inject
    public WeatherMapSharedPrefRepo(SharedPreferences sp, SharedPreferences unsecure_sp) {
        this.sp = sp;
        this.unsecure_sp = unsecure_sp;
    }

    public void clearAll() {

        sp.edit().clear().apply();
    }

    public Map<String, ?> getAllPreferences() {
        return sp.getAll();
    }

    public String getEmailAddress() {
        return sp.getString(KEY_EMAIL, DEFAULT);
    }

    //decided not to use these as firebase auth has the info. todo remove
    public void setEmailAddress(String emailAddress) {
        sp.edit().putString(KEY_EMAIL, emailAddress).apply();
    }
}
