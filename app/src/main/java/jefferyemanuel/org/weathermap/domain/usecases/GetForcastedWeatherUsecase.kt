package jefferyemanuel.org.weathermap.domain.usecases

import android.location.Location
import io.reactivex.Observable
import jefferyemanuel.org.weathermap.data.repositories.WeatherDataRepository
import jefferyemanuel.org.weathermap.domain.model.WeatherForcastModel
import jefferyemanuel.org.weathermap.domain.usecases.base.BaseUseCase
import javax.inject.Inject


class GetForcastedWeatherUsecase @Inject constructor(private var weatherDataRepository: WeatherDataRepository) : BaseUseCase() {

     var location: Location? = null

    override fun buildUseCaseObservable(): Observable<WeatherForcastModel>? {
        return weatherDataRepository.fetchWeatherForcastData(location)
    }

}