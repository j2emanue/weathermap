package jefferyemanuel.org.weathermap.domain.usecases

import android.location.Location
import io.reactivex.Observable
import jefferyemanuel.org.weathermap.data.repositories.WeatherDataRepository
import jefferyemanuel.org.weathermap.domain.model.WeatherDataModel
import jefferyemanuel.org.weathermap.domain.usecases.base.BaseUseCase
import javax.inject.Inject


class GetWeatherUpdateUsecase @Inject constructor(private var weatherDataRepository: WeatherDataRepository) : BaseUseCase() {

    lateinit var mLocation: Location
    override fun buildUseCaseObservable(): Observable<WeatherDataModel>? {

        return weatherDataRepository.fetchWeatherDataForLocation(mLocation)
    }

    //fun setLocation(location: Location) = apply { this.location = location }

}
