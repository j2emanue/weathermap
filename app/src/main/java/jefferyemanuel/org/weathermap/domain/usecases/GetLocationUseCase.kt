package jefferyemanuel.org.weathermap.domain.usecases

import android.annotation.SuppressLint
import android.location.Location
import com.google.android.gms.location.LocationRequest
import com.patloew.rxlocation.RxLocation
import io.reactivex.Observable
import jefferyemanuel.org.weathermap.domain.usecases.base.BaseUseCase
import javax.inject.Inject


class GetLocationUseCase @Inject constructor(var rxLocation: RxLocation) : BaseUseCase() {
    private lateinit var locationRequest: LocationRequest

    override fun buildUseCaseObservable(): Observable<Location> {
        return rxLocation.settings().checkAndHandleResolution(locationRequest)
                .toObservable().flatMap<Location> { getLocationObservable(it) }.firstOrError().toObservable()
    }

    @SuppressLint("MissingPermission")
    private fun getLocationObservable(success: Boolean): Observable<Location> {
        return if (success) {
            rxLocation.location().isLocationAvailable.flatMapObservable {
                if (it) rxLocation.location().lastLocation().toSingle().toObservable()
                else {
                    rxLocation.location().updates(locationRequest)
                }
            }
        } else {
            Observable.error(Throwable("Please check location settings"))
        }

    }

    fun setLocationRequest(locationRequest: LocationRequest) = apply { this.locationRequest = locationRequest }

}