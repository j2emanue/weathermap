package jefferyemanuel.org.weathermap.domain.usecases.base

//incase i need a pair of data returned for two network calls that depened on each other
//i usually like to work with pairs of data if there are multiple network calls. this might not be needed

class Pair<K, V>(val first: K, val second: V) {

    fun first(): K {
        return first
    }

    fun second(): V {
        return second
    }

    companion object {
    //static create method for convenience
        fun <K, V> create(first: K, second: V): Pair<K, V> {
            return Pair(first, second)
        }
    }

}
