package jefferyemanuel.org.weathermap.domain.model


data class WeatherForcastModel(
		var cod: String? = "",
		var message: Double? = 0.0,
		var cnt: Int? = 0,
		var list: List<Item7?>? = listOf(),
		var city: City? = City()
)

data class City(
		var id: Int? = 0,
		var name: String? = "",
		var coord: Coord? = Coord(),
		var country: String? = ""
)


data class Item7(
		var dt: Int? = 0,
		var main: Main? = Main(),
		var weather: List<Weather?>? = listOf(),
		var clouds: Clouds? = Clouds(),
		var wind: Wind? = Wind(),
		var rain: Rain? = Rain(),
		var sys: Sys? = Sys(),
		var dt_txt: String? = "",
		var day_formatted: String //added for easying day representation formatting
)

