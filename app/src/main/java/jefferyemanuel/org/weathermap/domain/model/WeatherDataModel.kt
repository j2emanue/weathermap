package jefferyemanuel.org.weathermap.domain.model


data class WeatherDataModel(
		var coord: Coord? = Coord(),
		var sys: Sys? = Sys(),
		var weather: List<Weather?>? = listOf(),
		var main: Main? = Main(),
		var wind: Wind? = Wind(),
		var rain: Rain? = Rain(),
		var clouds: Clouds? = Clouds(),
		var dt: Int? = 0,
		var id: Int? = 0,
		var name: String? = "",
		var cod: Int? = 0
)

data class Sys(
		var country: String? = "",
		var sunrise: Double? = 0.0,
		var sunset: Double? = 0.0
)

data class Clouds(
		var all: Int? = 0
)

data class Weather(
		var id: Int? = 0,
		var main: String? = "",
		var description: String? = "",
		var icon: String? = ""
)

data class Main(
		var temp: Double? = 0.0,
		var humidity: Double? = 0.0,
		var pressure: Double? = 0.0,
		var temp_min: Double? = 0.0,
		var temp_max: Double? = 0.0
)

data class Rain(
		var _3h: Int? = 0
)

data class Coord(
		var lon: Double? = 0.0,
		var lat: Double? = 0.0
)

data class Wind(
		var speed: Double? = 0.0,
		var deg: Double? = 0.0
)
