package jefferyemanuel.org.weathermap.frameworks.idlingResources;


import android.support.test.espresso.IdlingResource;
import android.support.v4.app.FragmentManager;

import timber.log.Timber;



public class LoadingDialogIdlingResource implements IdlingResource {
    private final FragmentManager manager;
    private final String tag;
    private ResourceCallback resourceCallback;

    public LoadingDialogIdlingResource(FragmentManager manager, String tag) {
        this.manager = manager;
        this.tag = tag;
    }

    @Override
    public String getName() {
        return LoadingDialogIdlingResource.class.getName() + ":" + tag;
    }

    @Override
    public boolean isIdleNow() {
        boolean idle = (manager.findFragmentByTag(tag) == null);
        Timber.d("idle:" + idle);

        if (idle) {
            resourceCallback.onTransitionToIdle();
        }
        return idle;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }
}
