package jefferyemanuel.org.weathermap.frameworks.rules;

import android.os.AsyncTask;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.util.concurrent.Callable;

import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

public class ThreadSchedulerRule implements TestRule {
    final Scheduler asyncTaskScheduler = Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR);

    @Override
    public Statement apply(final Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
        //https://gist.github.com/macsystems/4dab18dc363a4859e2e39215223a7b37
                RxJavaPlugins.setInitComputationSchedulerHandler(createSchedulingHandler());
                RxJavaPlugins.setInitIoSchedulerHandler(createSchedulingHandler());
                RxJavaPlugins.setInitNewThreadSchedulerHandler(createSchedulingHandler());
                try {
                    base.evaluate();
                } finally {
                    RxJavaPlugins.reset();
                }
            }
        };
    }


    @android.support.annotation.NonNull
    public Function<Callable<Scheduler>, Scheduler> createSchedulingHandler() {
        return new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Callable<Scheduler> schedulerCallable) throws Exception {
                return asyncTaskScheduler;
            }
        };
    }
}


