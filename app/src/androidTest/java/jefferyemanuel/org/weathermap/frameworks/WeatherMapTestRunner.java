package jefferyemanuel.org.weathermap.frameworks;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.test.runner.AndroidJUnitRunner;

import jefferyemanuel.org.weathermap.presentation.view.application.WeatherTestApplication;

public class WeatherMapTestRunner extends AndroidJUnitRunner {

    @Override
    public void onCreate(Bundle arguments) {
        super.onCreate(arguments);
    }

    @Override
    public Application newApplication(ClassLoader cl, String className, Context context) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        return super.newApplication(cl, WeatherTestApplication.class.getName(), context);
    }
}
