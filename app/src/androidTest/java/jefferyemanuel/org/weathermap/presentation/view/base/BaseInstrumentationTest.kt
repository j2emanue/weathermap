package jefferyemanuel.org.weathermap.presentation.view.base

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso
import android.support.test.espresso.IdlingRegistry
import android.support.test.espresso.ViewAssertion
import android.support.test.espresso.ViewInteraction
import android.support.v4.app.FragmentManager
import it.cosenonjaviste.daggermock.DaggerMockRule
import jefferyemanuel.org.weathermap.frameworks.dagger.components.TestAppComponent
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.ActivityModule
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.AppModule
import jefferyemanuel.org.weathermap.frameworks.dagger.modules.TestNetworkModule
import jefferyemanuel.org.weathermap.frameworks.idlingResources.LoadingDialogIdlingResource
import jefferyemanuel.org.weathermap.frameworks.idlingResources.OkHttpIdlingResource
import jefferyemanuel.org.weathermap.frameworks.idlingResources.PollingTimeoutIdler
import jefferyemanuel.org.weathermap.frameworks.rules.ThreadSchedulerRule
import jefferyemanuel.org.weathermap.presentation.view.application.WeatherApplication
import jefferyemanuel.org.weathermap.presentation.view.application.WeatherTestApplication
import jefferyemanuel.org.weathermap.presentation.view.customviews.dialogs.ProgressDialogFragment
import okhttp3.OkHttpClient
import org.junit.After
import org.junit.Rule




abstract class BaseInstrumentationTest {

    //this rule is for mocking any dagger graph objects easier using daggerMock. we dont have to use it but its there for convenience
    @Rule
    @JvmField val rule: DaggerMockRule<TestAppComponent> = DaggerMockRule(TestAppComponent::class.java, ActivityModule(),
            AppModule(app), TestNetworkModule())
    @Rule
    @JvmField var threadSchedulerRule = ThreadSchedulerRule()

    val app: WeatherApplication
        get() =
            InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as WeatherTestApplication


    private fun freeIdlingResources() {
        val idlingResourceList = IdlingRegistry.getInstance().getResources()
        if (idlingResourceList != null) {
            val iter = idlingResourceList.iterator()
            while (iter.hasNext()) {
                val res = iter.next()
                IdlingRegistry.getInstance().unregister(res)
            }
        }
    }

    fun setUpIdlingResources(okHttpClient: OkHttpClient, fm: FragmentManager) {
        val resource = OkHttpIdlingResource.create("OkHttp", okHttpClient)
        IdlingRegistry.getInstance().register(resource)

        val idlingResource = LoadingDialogIdlingResource(
                fm,
                ProgressDialogFragment::class.java.getName())

        IdlingRegistry.getInstance().register(idlingResource)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        //handle any clean up
        freeIdlingResources()
    }

    fun waitFor(viewInteraction: ViewInteraction, viewAssertion: ViewAssertion, timeout: Long) {

        val idler = PollingTimeoutIdler(viewInteraction, viewAssertion, timeout)
        Espresso.registerIdlingResources(idler)

        viewInteraction.check(viewAssertion)

        Espresso.unregisterIdlingResources(idler)
    }


    fun sleep(time: Long?) {
        try {
            Thread.sleep(time!!)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    inner class UITestHelper {
        //can put common testing functions calls here, like login/logout, clicking tabs, etc
    }

    companion object {
        val DEFAULT_TIME_UNTIL_VIEW_VISIBLE = 22000 //milliseconds
    }


}
