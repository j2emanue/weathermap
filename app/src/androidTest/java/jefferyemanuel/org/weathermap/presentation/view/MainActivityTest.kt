package jefferyemanuel.org.weathermap.presentation.view

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.intent.rule.IntentsTestRule
import android.support.test.filters.FlakyTest
import it.cosenonjaviste.daggermock.InjectFromComponent
import jefferyemanuel.org.weathermap.frameworks.rules.ThreadSchedulerRule
import jefferyemanuel.org.weathermap.presentation.view.base.BaseInstrumentationTest
import jefferyemanuel.org.weathermap.presentation.view.controllers.weather.activity.MainActivity
import okhttp3.OkHttpClient
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test


class MainActivityTest : BaseInstrumentationTest() {
    @Rule @JvmField  val mIntentsTestRule: IntentsTestRule<MainActivity> = object : IntentsTestRule<MainActivity>(MainActivity::class.java) {

        override fun beforeActivityLaunched() {
            super.beforeActivityLaunched()
        }

        override fun getActivityIntent(): Intent {
            val targetContext = InstrumentationRegistry.getInstrumentation()
                    .targetContext

            val i = Intent(targetContext, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
            return i
        }
    }

    @InjectFromComponent //must be exposed from dagger component to work
    lateinit var okHttpClient: OkHttpClient

    @Before
    fun setUp() {
        syncIdlingResources()
    }

    @Test
    @FlakyTest
    fun testWeatherDataAppears() {
    }

    fun syncIdlingResources() {
        setUpIdlingResources(okHttpClient, mIntentsTestRule.activity.supportFragmentManager)
    }

    companion object {

        @ClassRule
        @JvmField
        val threadSchedulerRule = ThreadSchedulerRule()
    }
}